[#ftl]
<!DOCTYPE html PUBLIC
        "-//OPENHTMLTOPDF//DOC XHTML Character Entities Only 1.0//EN" "">
<html lang="ru">
<head>
    <title>Unknown</title>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
    <style type="text/css">
        .temp-cont {
            font-size: 7px;
            border: #8f2ecf 1px solid;
            width: 350px;
            margin: auto;
        }

        sup {
            vertical-align: baseline;
            position: relative;
            top: -0.4em;
        }

        sub {
            vertical-align: baseline;
            position: relative;
            top: 0.4em;
        }

        a:link {
            text-decoration: none;
        }

        a:visited {
            text-decoration: none;
        }

        @media screen and (min-device-pixel-ratio: 0), (-webkit-min-device-pixel-ratio: 0), (min--moz-device-pixel-ratio: 0) {
            .stl_view {
                font-size: 10em;
                transform: scale(0.1);
                -moz-transform: scale(0.1);
                -webkit-transform: scale(0.1);
                -moz-transform-origin: top left;
                -webkit-transform-origin: top left;
            }
        }

        .layer {
        }

        .ie {
            font-size: 1pt;
        }

        .ie body {
            font-size: 12em;
        }

        @media print {
            .stl_view {
                font-size: 1em;
                transform: scale(1);
            }
        }

        .grlink {
            position: relative;
            width: 100%;
            height: 100%;
            z-index: 1000000;
        }

        .stl_01 {
            position: absolute;
            white-space: nowrap;
        }

        .stl_02 {
            font-size: 1em;
            line-height: 0.0em;
            width: 48.25em;
            height: 70.08334em;
            border-style: none;
            display: block;
            margin: 0em;
        }

        @supports (-ms-ime-align:auto) {
            .stl_02 {
                overflow: hidden;
            }
        }

        .stl_03 {
            position: relative;
        }

        .stl_04 {
            position: absolute;
            left: 0em;
            top: 0em;
        }

        .stl_05 {
            position: relative;
            width: 48.25em;
        }

        .stl_06 {
            height: 7.008333em;
        }

        .ie .stl_06 {
            height: 70.08334em;
        }

        .stl_07 {
            font-size: 2em;
            font-family: "PFRFCA+Raleway-Light", serif;
            color: #000000;
        }

        .stl_08 {
            line-height: 1.174em;
        }

        .stl_09 {
            letter-spacing: 0.0797em;
        }

        .ie .stl_09 {
            letter-spacing: 2.5493px;
        }


        .stl_10 {
            font-size: 3.333333em;
            font-family: "KWMSUI+Raleway-Bold", "Times New Roman", serif;
            color: #000000;
        }

        .stl_11 {
            letter-spacing: 0em;
        }

        .ie .stl_11 {
            letter-spacing: 0px;
        }


        .stl_12 {
            font-size: 1.166667em;
            font-family: "AFCOPF+Raleway-SemiBold", serif;
            color: #000000;
        }

        .stl_13 {
            font-size: 0.666667em;
            font-family: "PFRFCA+Raleway-Light", serif;
            color: #000000;
        }

        .stl_14 {
            letter-spacing: 0.0915em;
        }

        .ie .stl_14 {
            letter-spacing: 0.976px;
        }

        .stl_15 {
            letter-spacing: 0.0953em;
        }

        .ie .stl_15 {
            letter-spacing: 1.0164px;
        }

        .stl_16 {
            letter-spacing: 0.0976em;
        }

        .ie .stl_16 {
            letter-spacing: 1.0408px;
        }

        .stl_17 {
            letter-spacing: 0.098em;
        }

        .ie .stl_17 {
            letter-spacing: 1.0457px;
        }

        .stl_18 {
            letter-spacing: 0.099em;
        }

        .ie .stl_18 {
            letter-spacing: 1.0558px;
        }

        .stl_19 {
            letter-spacing: 0.0983em;
        }

        .ie .stl_19 {
            letter-spacing: 1.0487px;
        }

        .stl_20 {
            letter-spacing: 0.0974em;
        }

        .ie .stl_20 {
            letter-spacing: 1.0384px;
        }

        .stl_21 {
            letter-spacing: 0.0994em;
        }

        .ie .stl_21 {
            letter-spacing: 1.0599px;
        }

        .stl_22 {
            letter-spacing: 0.1em;
        }

        .ie .stl_22 {
            letter-spacing: 1.0667px;
        }

        .stl_23 {
            letter-spacing: 0.0909em;
        }

        .ie .stl_23 {
            letter-spacing: 0.9691px;
        }

        .stl_24 {
            font-size: 0.833333em;
            font-family: "AFCOPF+Raleway-SemiBold", serif;
            color: #545456;
        }

        .stl_25 {
            letter-spacing: 0.0982em;
        }

        .ie .stl_25 {
            letter-spacing: 1.0478px;
        }

        .stl_26 {
            letter-spacing: 0.0936em;
        }

        .ie .stl_26 {
            letter-spacing: 0.9982px;
        }

        .stl_27 {
            letter-spacing: 0.0995em;
        }

        .ie .stl_27 {
            letter-spacing: 1.0615px;
        }

        .stl_28 {
            letter-spacing: 0.0963em;
        }

        .ie .stl_28 {
            letter-spacing: 1.0267px;
        }

        .stl_29 {
            letter-spacing: 0.0969em;
        }

        .ie .stl_29 {
            letter-spacing: 1.0335px;
        }

        .stl_30 {
            letter-spacing: 0.0959em;
        }

        .ie .stl_30 {
            letter-spacing: 1.0229px;
        }

        .stl_31 {
            letter-spacing: -0.01em;
        }

        .ie .stl_31 {
            letter-spacing: -0.1067px;
        }

        .stl_32 {
            letter-spacing: 0.093em;
        }

        .ie .stl_32 {
            letter-spacing: 0.992px;
        }

        .stl_33 {
            letter-spacing: 0.0893em;
        }

        .ie .stl_33 {
            letter-spacing: 0.9529px;
        }

        .stl_34 {
            letter-spacing: 0.0979em;
        }

        .ie .stl_34 {
            letter-spacing: 1.0443px;
        }

        .stl_35 {
            letter-spacing: 0.0938em;
        }

        .ie .stl_35 {
            letter-spacing: 1.0009px;
        }

    </style>
</head>
<body>


<div>
    <div class="stl_02">
        <div class="stl_03">
            <object data="img_01.svg" type="image/svg+xml" class="stl_04">
                <embed src="img_01.svg" type="image/svg+xml"/>
            </object>
        </div>
        <div class="stl_view">
            <div class="stl_05 stl_06">
                <div class="stl_01" style="left:3em;top:6.6925em;"><span
                            class="stl_07 stl_08 stl_09">[#if user.firstName??]${user.firstName}[/#if] &nbsp;</span>
                </div>
                <div class="stl_01" style="left:3.0417em;top:8.8425em;"><span class="stl_10 stl_08 stl_11"
                                                                              style="word-spacing:-0.0353em;">[#if user.lastName??]${user.lastName}[/#if] &nbsp;</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:18.9667em;"><span class="stl_12 stl_08 stl_11"
                                                                               style="word-spacing:0.0347em;">C O N T A C T &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.8em;top:18.9712em;"><span class="stl_12 stl_08 stl_11"
                                                                             style="word-spacing:0.053em;">P R O F I L E &nbsp;</span>
                </div>
                <div class="stl_01" style="left:3.7083em;top:21.9615em;"><span class="stl_13 stl_08 stl_14"
                                                                               style="word-spacing:0.017em;">Avenue Park NY-0123-USA &nbsp;</span>
                </div>
                <div class="stl_01" style="left:3.7083em;top:23.1908em;"><span class="stl_13 stl_08 stl_15"
                                                                               style="word-spacing:0.0094em;">+44 458-2356-2356 &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.7997em;top:21.9615em;"><span class="stl_13 stl_08 stl_16"
                                                                                style="word-spacing:0.0018em;">Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.7997em;top:22.7615em;"><span class="stl_13 stl_08 stl_17"
                                                                                style="word-spacing:-0.0051em;">orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.7997em;top:23.5615em;"><span class="stl_13 stl_08 stl_18"
                                                                                style="word-spacing:-0.0004em;">porehenit plictur as et, sit liquasped quiatquam, sectur? Quiatis itatis eiciet &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.7997em;top:24.3615em;"><span class="stl_13 stl_08 stl_19"
                                                                                style="word-spacing:0.0034em;">aut et fuga. Itamuscim reptasp. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:3.7083em;top:24.3908em;"><a href="mailto:sam.william@gmail.com"
                                                                            target="_blank"><span
                                class="stl_13 stl_08 stl_20">sam.william@gmail.com &nbsp;</span></a></div>
                <div class="stl_01" style="left:2.9583em;top:26.0215em;"><span class="stl_12 stl_08 stl_11"
                                                                               style="word-spacing:0.053em;">S K I L L S &nbsp;</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:28.2215em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:29.7215em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:31.2215em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:32.7215em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:34.2215em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:4.4583em;top:28.2215em;"><span class="stl_13 stl_08 stl_21"
                                                                               style="word-spacing:0.0013em;">Graphic Design &nbsp;</span>
                </div>
                <div class="stl_01" style="left:18.8em;top:28.6091em;"><span class="stl_12 stl_08 stl_11"
                                                                             style="word-spacing:0.053em;">E X P E R I E N C E &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4583em;top:29.7215em;"><span class="stl_13 stl_08 stl_22"
                                                                               style="word-spacing:-0.012em;">UI/UX Design &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4583em;top:31.2215em;"><span class="stl_13 stl_08 stl_23"
                                                                               style="word-spacing:0.0183em;">Web Design &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.2em;top:31.4075em;"><span class="stl_24 stl_08 stl_11"
                                                                             style="word-spacing:0.053em;">J O B</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.572em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">T I T L E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">H E R E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">| 2</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">0 3 4</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">- P</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.05em;">R E S E N T &nbsp;</span></div>
                <div class="stl_01" style="left:20.2em;top:33.305em;"><span class="stl_24 stl_08 stl_11"
                                                                            style="word-spacing:0.0443em;">C O M P A N Y</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.566em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">N A M E &nbsp;</span></div>
                <div class="stl_01" style="left:20.1997em;top:34.5928em;"><span class="stl_13 stl_08 stl_16"
                                                                                style="word-spacing:0.0018em;">Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:35.3928em;"><span class="stl_13 stl_08 stl_17"
                                                                                style="word-spacing:-0.0051em;">orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:36.1928em;"><span class="stl_13 stl_08 stl_25"
                                                                                style="word-spacing:-0.0045em;">porehenit plictur as et. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4583em;top:32.7215em;"><span class="stl_13 stl_08 stl_16"
                                                                               style="word-spacing:0.0048em;">Project Management &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4583em;top:34.2215em;"><span class="stl_13 stl_08 stl_26"
                                                                               style="word-spacing:0.0128em;">Market Research &nbsp;</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:36.2985em;"><span class="stl_12 stl_08 stl_11"
                                                                               style="word-spacing:0.0506em;">I N T E R E S T S &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:36.9928em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:37.7928em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:38.5928em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:36.9928em;"><span class="stl_13 stl_08 stl_27"
                                                                                style="word-spacing:-0.0062em;">Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:37.7928em;"><span class="stl_13 stl_08 stl_28"
                                                                                style="word-spacing:-0.0046em;">molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:38.5928em;"><span class="stl_13 stl_08 stl_29"
                                                                                style="word-spacing:0.0018em;">Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:38.6186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:40.1186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:41.6186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:43.1186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:44.6186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:46.1186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:47.6186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:8.6419em;top:49.1186em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:38.6186em;"><span class="stl_13 stl_08 stl_30">Photography &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:40.1186em;"><span class="stl_13 stl_08 stl_16">Swimming &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:41.6186em;"><span class="stl_13 stl_08 stl_18">Riding &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:43.1186em;"><span
                            class="stl_13 stl_08 stl_31">T</span><span class="stl_13 stl_08 stl_32">r</span><span
                            class="stl_13 stl_08 stl_33">avel &nbsp;</span></div>
                <div class="stl_01" style="left:20.2em;top:43.6233em;"><span class="stl_24 stl_08 stl_11"
                                                                             style="word-spacing:0.053em;">J O B</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.572em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">T I T L E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">H E R E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">| 2</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">0 3 0</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">- 2</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">0 3 4 &nbsp;</span></div>
                <div class="stl_01" style="left:20.2em;top:45.5208em;"><span class="stl_24 stl_08 stl_11"
                                                                             style="word-spacing:0.0443em;">C O M P A N Y</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.566em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">N A M E &nbsp;</span></div>
                <div class="stl_01" style="left:20.1997em;top:46.8081em;"><span class="stl_13 stl_08 stl_16"
                                                                                style="word-spacing:0.0018em;">Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:47.6081em;"><span class="stl_13 stl_08 stl_17"
                                                                                style="word-spacing:-0.0051em;">orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:48.4081em;"><span class="stl_13 stl_08 stl_25"
                                                                                style="word-spacing:-0.0045em;">porehenit plictur as et. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:44.6186em;"><span class="stl_13 stl_08 stl_34">Calligraphy &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:46.1186em;"><span class="stl_13 stl_08 stl_35">Writing &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:47.6186em;"><span class="stl_13 stl_08 stl_22">Blogging &nbsp;</span>
                </div>
                <div class="stl_01" style="left:10.1419em;top:49.1186em;"><span class="stl_13 stl_08 stl_30">Researching &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:49.2081em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:50.0081em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:50.8081em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:49.2081em;"><span class="stl_13 stl_08 stl_27"
                                                                                style="word-spacing:-0.0062em;">Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:50.0081em;"><span class="stl_13 stl_08 stl_28"
                                                                                style="word-spacing:-0.0046em;">molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:50.8081em;"><span class="stl_13 stl_08 stl_29"
                                                                                style="word-spacing:0.0018em;">Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:2.9583em;top:52.1385em;"><span class="stl_12 stl_08 stl_11"
                                                                               style="word-spacing:0.043em;">E D U C A T I O N &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4142em;top:54.3541em;"><span class="stl_24 stl_08 stl_11"
                                                                               style="word-spacing:0.05em;">D E G R E E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.572em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">T I T L E &nbsp;</span></div>
                <div class="stl_01" style="left:4.4143em;top:55.8728em;"><span class="stl_13 stl_08 stl_22"
                                                                               style="word-spacing:-0.034em;">UNIVERSITY NAME &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4143em;top:56.6728em;"><span class="stl_13 stl_08 stl_27"
                                                                               style="word-spacing:-0.023em;">Major Subject &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4143em;top:57.4728em;"><span class="stl_13 stl_08 stl_22"
                                                                               style="word-spacing:0em;">2030 - 2034 &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.2em;top:55.8391em;"><span class="stl_24 stl_08 stl_11"
                                                                             style="word-spacing:0.053em;">J O B</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.572em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">T I T L E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">H E R E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">| 2</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">0 3 0</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.6em;">- 2</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">0 3 4 &nbsp;</span></div>
                <div class="stl_01" style="left:20.2em;top:57.7366em;"><span class="stl_24 stl_08 stl_11"
                                                                             style="word-spacing:0.0443em;">C O M P A N Y</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.566em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">N A M E &nbsp;</span></div>
                <div class="stl_01" style="left:20.1997em;top:59.0241em;"><span class="stl_13 stl_08 stl_16"
                                                                                style="word-spacing:0.0018em;">Itae audi voluptatum audandant rest acilign atemporpore nes non pratemp &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:59.8241em;"><span class="stl_13 stl_08 stl_17"
                                                                                style="word-spacing:-0.0051em;">orest, tem quost aut vernatius doluptae volum vellorrum inctaque ommolor &nbsp;</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:60.6241em;"><span class="stl_13 stl_08 stl_25"
                                                                                style="word-spacing:-0.0045em;">porehenit plictur as et. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4142em;top:60.6641em;"><span class="stl_24 stl_08 stl_11"
                                                                               style="word-spacing:0.05em;">D E G R E E</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.572em;">&nbsp;</span><span
                            class="stl_24 stl_08 stl_11" style="word-spacing:0.053em;">T I T L E &nbsp;</span></div>
                <div class="stl_01" style="left:20.1997em;top:61.4241em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:62.2241em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:20.1997em;top:63.0241em;"><span class="stl_13 stl_08 stl_11">•</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:61.4241em;"><span class="stl_13 stl_08 stl_27"
                                                                                style="word-spacing:-0.0062em;">Por a duntur aut qui omnienist amene nobitatur Quia si aut &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:62.2241em;"><span class="stl_13 stl_08 stl_28"
                                                                                style="word-spacing:-0.0046em;">molute vel exerior susdaes volupta ectiis et liquas solorep tatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:21.6997em;top:63.0241em;"><span class="stl_13 stl_08 stl_29"
                                                                                style="word-spacing:0.0018em;">Quiae eaquis apienet rerro evenimus parunt magnihil modiatur. &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4143em;top:62.1828em;"><span class="stl_13 stl_08 stl_22"
                                                                               style="word-spacing:-0.034em;">UNIVERSITY NAME &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4143em;top:62.9828em;"><span class="stl_13 stl_08 stl_27"
                                                                               style="word-spacing:-0.023em;">Major Subject &nbsp;</span>
                </div>
                <div class="stl_01" style="left:4.4143em;top:63.7828em;"><span class="stl_13 stl_08 stl_22"
                                                                               style="word-spacing:0em;">2030 - 2034 &nbsp;</span>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>