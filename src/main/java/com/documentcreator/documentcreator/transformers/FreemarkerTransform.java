package com.documentcreator.documentcreator.transformers;

import freemarker.cache.ByteArrayTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.Template;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;

import java.io.ByteArrayOutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.Map;

@Component
public class FreemarkerTransform implements DocumentTransformer {

    @SneakyThrows
    public byte[] transform(byte[] byteArrayTemplate, Map<String, Object> attributes) {
        ByteArrayTemplateLoader templateLoader = new ByteArrayTemplateLoader();
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        templateLoader.putTemplate("doc", byteArrayTemplate);

        Configuration cfg = new Configuration(Configuration.VERSION_2_3_27);
        cfg.setTemplateLoader(templateLoader);

        Template template = cfg.getTemplate("doc");
        Writer outWriter = new OutputStreamWriter(outputStream);
        template.process(attributes, outWriter);
        return outputStream.toByteArray();
    }

    @Override
    public String getExtension() {
        return ".ftl";
    }
}
