package com.documentcreator.documentcreator.transformers;

public enum DocumentType {
    PDF,
    DOCX,
    HTML
}
