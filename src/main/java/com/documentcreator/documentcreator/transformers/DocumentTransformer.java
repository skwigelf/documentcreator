package com.documentcreator.documentcreator.transformers;

import java.util.Map;

/**
 * Компонент для преобразования документов (например шаблонизатор)
 */
public interface DocumentTransformer {

    /**
     * Преобразует документ для указанного аттрибутного состава в соответствии с указанным в компоненте трансформером
     * @param byteArrayTemplate исходный документ в виде byte[]
     * @param attributes Аттрибутный состав
     * @return Преобразованный документ
     */
    byte[] transform(byte[] byteArrayTemplate, Map<String, Object> attributes);

    /**
     * Получить значение расшерения для шаблонов данного трансформера
     * @return Расширение трансформера
     */
    String getExtension();
}
