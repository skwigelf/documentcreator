package com.documentcreator.documentcreator.service;

import com.documentcreator.documentcreator.dto.DocumentDto;

import java.util.Map;

public interface DocumentService {
    /**
     * Создать документ из указанного шаблона и атрибутов
     *
     * @param attributes   аттрибуты шаблонизатора
     * @param templateName имя шаблона
     * @return Преобразованный документ
     */
    DocumentDto createDocument(Map<String, Object> attributes, String templateName);

    /**
     * Получить шаблон документа
     *
     * @param templateName имя шаблона
     * @return Документ
     */
    DocumentDto getDocumentTemplate(String templateName);
}
