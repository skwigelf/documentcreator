package com.documentcreator.documentcreator.service;

import com.documentcreator.documentcreator.transformers.DocumentTransformer;
import com.documentcreator.documentcreator.converters.PdfConverter;
import com.documentcreator.documentcreator.dto.DocumentDto;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class DocumentServiceImpl implements DocumentService {
    private final PdfConverter pdfConverter;
    private final DocumentTransformer freemarkerTransform;

    private static final String TEMPLATES_PATH = "/templates";

    @SneakyThrows
    @Override
    public DocumentDto createDocument(Map<String, Object> attributes, String templateName) {
        byte[] freemarkerTemplate = getTemplateByName(templateName);

        byte[] freemarkerResolved = freemarkerTransform.transform(freemarkerTemplate, attributes);
        return pdfConverter.convert(freemarkerResolved);
    }

    @SneakyThrows
    @Override
    public DocumentDto getDocumentTemplate(String templateName) {
        return pdfConverter.convert(getTemplateByName(templateName));
    }

    private byte[] getTemplateByName(String templateName) throws IOException {
        return getClass().getResourceAsStream(
                TEMPLATES_PATH + "/" + templateName + freemarkerTransform.getExtension()).readAllBytes();
    }
}
