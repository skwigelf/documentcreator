package com.documentcreator.documentcreator.web;

import com.documentcreator.documentcreator.dto.DocumentDto;
import com.documentcreator.documentcreator.service.DocumentService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.Map;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.springframework.http.ContentDisposition.builder;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;

@RestController
@RequestMapping("/api/rest/docs")
@CrossOrigin(origins = "http://localhost:3000")
@RequiredArgsConstructor
public class DocumentCreatorController {
    private final DocumentService documentService;

    @PostMapping("/{templateId}")
    public ResponseEntity<byte[]> createDocument(@RequestBody Map<String, Object> data, @PathVariable String templateId) {
        DocumentDto documentDto = documentService.createDocument(data, templateId);
        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, builder("attachment").filename(documentDto.getFileName(), UTF_8).build().toString())
                .header(CONTENT_TYPE, documentDto.getMediaType())
                .body(documentDto.getContent());
    }

    @GetMapping("/{templateId}")
    public ResponseEntity<byte[]> getTemplate(@PathVariable String templateId) {
        DocumentDto documentDto = documentService.getDocumentTemplate(templateId);
        return ResponseEntity.ok()
                .header(CONTENT_DISPOSITION, builder("attachment").filename(documentDto.getFileName(), UTF_8).build().toString())
                .header(CONTENT_TYPE, documentDto.getMediaType())
                .body(documentDto.getContent());
    }
}
