package com.documentcreator.documentcreator.converters;

import com.documentcreator.documentcreator.dto.DocumentDto;
import com.openhtmltopdf.outputdevice.helper.BaseRendererBuilder;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import lombok.SneakyThrows;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import java.awt.*;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static java.awt.Font.createFont;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

@Component
public class PdfConverter implements Converter {

    private static final Logger LOGGER = LoggerFactory.getLogger(PdfConverter.class);
    private static final String FONTS_PATH = "fonts";

    private PdfRendererBuilder pdfRendererBuilder;

    public PdfConverter() {
        pdfRendererBuilder = new PdfRendererBuilder();
        try (Stream<Path> paths = Files.walk(Paths.get(getFileUrl(FONTS_PATH)))) {
            paths
                    .filter(Files::isRegularFile)
                    .filter(this::filterTrueTypeFonts)
                    .map(this::createInputStreamByPath)
                    .forEach(inputStream ->
                            pdfRendererBuilder.useFont(
                                    () -> inputStream,
                                    getFontFamily(inputStream),
                                    400,
                                    BaseRendererBuilder.FontStyle.NORMAL,
                                    true
                            )
                    );

        } catch (IOException e) {
            throw new IllegalStateException("Error creating pdf renderer builder", e);
        }

    }

    @SneakyThrows
    @Override
    public DocumentDto convert(byte[] content) {
        LOGGER.debug("Document convertion to pdf started");
        ByteArrayOutputStream pdfOutputStream = new ByteArrayOutputStream();
        ByteArrayOutputStream pdf2OutputStream = new ByteArrayOutputStream();
        pdfRendererBuilder
                .withHtmlContent(new String(content, UTF_8), "")
                .toStream(pdfOutputStream)
                .buildPdfRenderer()
                .createPDF();
        PDDocument document = PDDocument.load(pdfOutputStream.toByteArray());
        String title = document.getDocumentInformation().getTitle();
        document.save(pdf2OutputStream);
        byte[] pdfContent = pdf2OutputStream.toByteArray();
        document.close();
        LOGGER.debug("Document convertion to pdf finished");

        return new DocumentDto(MediaType.APPLICATION_PDF_VALUE, pdfContent, title);
    }

    private String getFileUrl(String path) {
        return requireNonNull(getClass().getClassLoader().getResource(path)).getFile();
    }

    private InputStream createInputStreamByPath(Path path) {
        try {
            return new FileInputStream(new File(path.toString()));
        } catch (Exception e) {
            throw new IllegalStateException("No file", e);
        }
    }

    private boolean filterTrueTypeFonts(Path path) {
        String extension = getExtensionFromPath(path);
        return extension.equals(".ttf");
    }

    private String getExtensionFromPath(Path path) {
        String filename = path.getFileName().toString();
        Pattern pattern = Pattern.compile("(\\..*)");
        Matcher matcher = pattern.matcher(filename);
        if (matcher.find()) {
            return matcher.group(0);
        } else {
            throw new IllegalStateException("Error capturing extension from filename: " + filename);
        }
    }

    @SneakyThrows
    private String getFontFamily(InputStream inputStream) {
        return createFont(Font.TRUETYPE_FONT, requireNonNull(inputStream)).getFamily();
    }
}
