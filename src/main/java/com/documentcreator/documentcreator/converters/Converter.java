package com.documentcreator.documentcreator.converters;

import com.documentcreator.documentcreator.dto.DocumentDto;

/**
 * Конвертер документов
 */
public interface Converter {
    /**
     * Конвертирует указанный документ
     *
     * @param content исходный документ в виде byte[]
     * @return Документ в новом формате, определнном в данном конвертере
     */
    DocumentDto convert(byte[] content);
}
